# Summary

Application that gets data from NASDAQ and provides access to these data through RESTful API.

This application is based on the following technologies: Java EE, JAX-RS, JPA.

# Tools

* Version Control - [Git 2.6.x](https://git-scm.com/)
* Build Tool - [Maven 3.3.x](https://maven.apache.org/)
* Application Server - [JBoss EAP 6.4.0.GA](http://www.jboss.org/products/eap/download/)
* Database - [MySQL 5.7.x](http://dev.mysql.com/downloads/mysql/)

# Modules

Contains modules of application:

* `nasdaq-scraper-web` - module for web application.
* `nasdaq-scraper-ear` - module to build EAR file.

# Local

Scripts for local environment:

* `app-build` - build application.
* `app-deploy` - deploy application on AS.
* `db-clean` - remove all data from local DB.
* `db-migrate` - apply migrations (from `local/config/mysql/migrations`) to local DB.

Configuration files:

* `config/jboss` - configuration files for JBoss.
* `config/mysql` - migrations for MySQL.
