package hrytsenko.nasdaq.daemon;

import java.util.logging.Logger;

import javax.interceptor.AroundTimeout;
import javax.interceptor.InvocationContext;

import com.google.common.base.Stopwatch;

public class DaemonInterceptor {

    private static final Logger LOGGER = Logger.getLogger(DaemonInterceptor.class.getName());

    @AroundTimeout
    public Object log(InvocationContext context) throws Exception {
        String name = context.getTarget().getClass().getSimpleName();

        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            return context.proceed();
        } catch (Exception exception) {
            LOGGER.severe(() -> String.format("Invocation of %s failed.", name));
            throw exception;
        } finally {
            LOGGER.info(() -> String.format("Invocation of %s completed in %s.", name, stopwatch));
        }
    }

}
