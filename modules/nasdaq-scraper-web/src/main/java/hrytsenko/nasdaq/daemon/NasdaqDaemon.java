package hrytsenko.nasdaq.daemon;

import hrytsenko.nasdaq.domain.CompaniesService;
import hrytsenko.nasdaq.endpoint.NasdaqEndpoint;
import hrytsenko.nasdaq.system.SettingsService;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

@Startup
@Singleton
@Interceptors(DaemonInterceptor.class)
public class NasdaqDaemon {

    private static final Logger LOGGER = Logger.getLogger(NasdaqDaemon.class.getName());

    @Resource
    private TimerService timerService;

    @Inject
    private SettingsService settingsService;
    @Inject
    private CompaniesService companiesService;
    @Inject
    private NasdaqEndpoint nasdaqEndpoint;

    @PostConstruct
    public void initDaemon() {
        TimerConfig config = new TimerConfig(getClass().getSimpleName(), false);
        timerService.createSingleActionTimer(0, config);
        timerService.createCalendarTimer(new ScheduleExpression().hour("*").minute("*/30"), config);
    }

    @Timeout
    public void updateCompanies() {
        settingsService.getExchanges().stream().forEach(this::updateCompanies);
    }

    private void updateCompanies(String exchange) {
        LOGGER.info(() -> String.format("Update companies for %s.", exchange));
        nasdaqEndpoint.downloadCompanies(exchange).forEach(companiesService::updateCompany);
    }

}
